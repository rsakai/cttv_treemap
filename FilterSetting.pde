class FilterSetting{
	int lens_index; //index of lens to be filtered
	String filter_value; //filtered value
	String lens_name; //name of lens

	Rectangle display_rect;
	String filter_result = "";


	int btn_size = 15;

	FilterSetting(int lens_index, String lens_name, String filter_value){
		this.lens_index = lens_index;
		this.lens_name = lens_name;
		this.filter_value = filter_value;
	}

	String toString(){
		return lens_name+"/"+filter_value;
	}


	//check if the link matches the filter setting
	boolean match(Link l){
		if(lens_index == L_TYPE){
			//check in the hashmap
			//look up HashMap for a function if there is a specific key name
			return l.datasource_map.containsKey(filter_value);
		}else{
			String value = getItemName(l, lens_index);
			return value.equals(filter_value);
		}


	}

	boolean contain_delete(int mx, int my){
		return(mx > display_rect.x+display_rect.width-btn_size && mx <display_rect.x+display_rect.width &&
			my > display_rect.y && my < display_rect.y+btn_size);
	}

	Rectangle get_delete_rect(){
		return new Rectangle(display_rect.x+display_rect.width-btn_size, display_rect.y, btn_size, btn_size);
	}
}