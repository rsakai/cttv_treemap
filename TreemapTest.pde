import treemap.*;
import java.awt.*;
import java.util.*;


Treemap map, detail_map;
MapLayout layoutAlgorithm = new PivotBySplitSize();
EvidenceMap firstMap;
EvidenceItem rolloverItem = null;



int maxFontSize = 1000;
int minFontSize = 1;

PFont font;
PFont font2; //text


int _margin = 10;
int _top_margin = _margin*2;
int _bottom_margin = _margin *2;
int _left_margin = _margin*5;
int _right_margin = _margin*5;
int _stage_width, _stage_height;
int _window_bar_height = 50;


float _map_ratio = 8;
float _gap_ratio = 1;
float _list_ratio = 2;
float _total_ratio = _map_ratio + _gap_ratio + _list_ratio;
int list_gap = _margin*5; //gap between the list and bredcrumb

Rectangle _map_rect, _gap_rect, _list_rect, _breadcrumb_rect;


//optimize cpu
int draw_counter = 0;
int draw_max = 30; //1 seconds = draw 30 times before it stops

//data
HashMap<String, Gene> gene_map;
ArrayList<Gene> gene_array;
HashMap<String, Disease> disease_map;
ArrayList<Disease> disease_array;
HashMap<String, DiseaseGroup> disease_group_map;
ArrayList<DiseaseGroup> disease_group_array;
HashSet<String> source_set;
ArrayList<String> source_array;
ArrayList<Link> link_array;

ArrayList<Link> selected_evidence;

//filter hierarchy
ArrayList<FilterSetting> filter_array;


//filter setting
String[] lens_names = {
	"gene_name", 
	"gene_name_weighted",
	"gene_group", 
	"disease_toplevel",
	"disease_name", 
	"disease_name_weighted", 
	"disease_isRare", 
	"evidence_type_count", 
	"evidence_type",
	"total_hit_count", 
};

String[] lens_description = {
	"gene_name: counting the number of links by gene name.", 
	"gene_name_weighted: the area reflect the number of evidence/hits",
	"gene_group: aggregate by gene group index", 
	"disease_toplevel: higher level EFO",
	"disease_name: lower level EFO and counting the number of links", 
	"disease_name_weighted: lower level EFO and the number of hits/confidence", 
	"disease_isRare: boolean whether common or rare", 
	"evidence_type_count: aggregate by the number of data source overlaps", 
	"evidence_type: agrregate by the data source name. For overlaping evidence, they are counted multiple times.",
	"total_hit_count: sum of hits of differnt for the link.", 
};


// String[] lens_names = {"gene_name", "gene_group", "disease_name", "disease_toplevel",
// 					 "disease_isRare", "link_source", "link_value", "disease_name_value",
// 					 "disease_toplevel_value"};
static final int G_NAME = 0;
static final int G_NAME_W = 1;
static final int G_GROUP = 2;
static final int D_TOP = 3;
static final int D_NAME = 4;
static final int D_NAME_W = 5;
static final int D_RARE = 6;
static final int L_TYPES = 7;
static final int L_TYPE = 8;
static final int L_TOTAL_HIT = 9;

static final int L_VALUE = 6;
static final int D_NAME_VALUE = 7;
static final int D_TOP_VALUE = 8;

int current_lens = G_NAME;//D_TOP;
int second_lens = G_NAME;

Rectangle[]  lens_label_rect, first_lens_rect, second_lens_rect;

//information text
Rectangle info_text_rect;
	

void setup() {
	//setup display
	_stage_width = displayWidth;
	_stage_height = displayHeight - _window_bar_height;

	int map_width = round((_stage_width - _left_margin - _right_margin)*(_map_ratio/_total_ratio));
	int map_height = round(_stage_height - _top_margin - _bottom_margin);
	_map_rect = new Rectangle(_left_margin, _top_margin, map_width, map_height);

	int gap_width = round((_stage_width - _left_margin - _right_margin)*(_gap_ratio/_total_ratio));
	_gap_rect = new Rectangle(_map_rect.x+_map_rect.width, 0, gap_width, _stage_height);

	int list_height = round((_stage_height - _top_margin - _bottom_margin - list_gap)/2);
	int list_width = round((_stage_width - _left_margin - _right_margin)*(_list_ratio/_total_ratio));
	_list_rect = new Rectangle(_gap_rect.x+_gap_rect.width, _top_margin, list_width, list_height);

	int bc_height = _stage_height - _top_margin - _bottom_margin -list_gap - list_height;
	_breadcrumb_rect = new Rectangle(_list_rect.x, _list_rect.y + _list_rect.height + list_gap, list_width, bc_height);

	size(_stage_width, _stage_height);

	//butttons in the list
	lens_label_rect = new Rectangle[lens_names.length];
	first_lens_rect = new Rectangle[lens_names.length];
	second_lens_rect = new Rectangle[lens_names.length];
	int runningY = _list_rect.y;
	int label_height = _margin*3;
	for(int i = 0; i < lens_names.length; i++){
		int runningX = _list_rect.x + _list_rect.width - label_height;
		second_lens_rect[i] = new Rectangle(runningX, runningY, label_height, label_height);
		runningX -= label_height;
		first_lens_rect[i] = new Rectangle(runningX, runningY, label_height, label_height);
		int label_width = runningX - _list_rect.x;
		runningX = _list_rect.x;
		lens_label_rect[i] = new Rectangle(runningX, runningY, label_width, label_height);
		runningY += label_height;
	}

	//info_text_rect

	info_text_rect = new Rectangle(0, _stage_height - _bottom_margin, _stage_width, _bottom_margin);


	font = createFont("miso-bold.ttf", 10);
	font2 = createFont("Supernatural1001", 15);

	smooth();

	//load data
	load_data();
	filter_array = new ArrayList<FilterSetting>();

	//starting data
	selected_evidence = new ArrayList();
	selected_evidence.addAll(link_array);


	updateTreemap();
	updateLayout();


	//  ------ read a textfile ------
	// String[] lines = loadStrings("Faust.txt");
	// // join all lines to a big string
	// String joinedText = join(lines, " ");

	// // replacings
	// joinedText = joinedText.replaceAll("_", "");  

	// // split tex into words by delimiters
	// String[] words = splitTokens(joinedText, " ¬ª¬´‚Äì_-–().,;:?!\u2014\"");

	// // add all words to the treemap
	// for (int i = 0; i < words.length; i++) {
	//   // translate all to UPPERCASE
	//   String word = words[i].toLowerCase();
	//   mapData.addWord(word);     
	// }

	// //  ------ treemap data is ready ------
	// mapData.finishAdd();

	// // create treemap with mapData
	// map = new Treemap(mapData, 0, 0, width, height);
}


void load_data(){
	// JSONArray datat = loadJSONArray("genes2diseases.json");
	// JSONObject data = loadJSONObject("genes2diseases.json");
	JSONObject data = loadJSONObject("genes2diseases_v2.json");


	//find all keys
	HashSet keys = new HashSet(data.keys());
	for(Object obj:keys){
		String key = (String)obj;
		println("debug: key = "+ key);
	}

	JSONArray links = data.getJSONArray("links");
	JSONArray gene_nodes = data.getJSONArray("gene_nodes");
	JSONArray disease_nodes = data.getJSONArray("disease_nodes");

	println("debug: "+links.size()+" links");
	println("\t"+links.getJSONObject(0).keys());
	println("debug: "+gene_nodes.size()+" genes");
	println("\t"+gene_nodes.getJSONObject(0).keys());
	println("debug: "+disease_nodes.size()+" diseases");
	println("\t"+disease_nodes.getJSONObject(0).keys());



	// print(links.toString());
	// println(gene_nodes.toString());
	// println(disease_nodes.toString());


	//parse genes
	gene_map = new HashMap<String, Gene>();
	gene_array = new ArrayList<Gene>();
	for (int i = 0; i < gene_nodes.size(); i++) {
		JSONObject obj = gene_nodes.getJSONObject(i);
		// println(obj);
		String name = "";
		String acc = obj.getString("acc");
		if(obj.isNull("name")){
			name = acc;
			println(acc);
		}else{
			name = obj.getString("name");
		}
		int group = obj.getInt("group");
		Gene gene = new Gene(name, acc, group, i);
		gene_array.add(gene);
		gene_map.put(name, gene);
		// println(obj);
		// println(i+"\t"+name);
	}
	println("debug: total number of genes = "+gene_array.size());


	//parse diseases
	disease_map = new HashMap<String, Disease>();
	disease_array = new ArrayList<Disease>();
	disease_group_map = new HashMap<String, DiseaseGroup>();
	disease_group_array = new ArrayList<DiseaseGroup>();
	for(int i = 0; i < disease_nodes.size(); i++){
		JSONObject obj = disease_nodes.getJSONObject(i);
		String url = obj.getString("id");
		String name = obj.getString("name");
		String top_level = "";

		if(obj.isNull("top_level")){
			//in the new dataset the top_levels is actually an array
			JSONArray top_levels = obj.getJSONArray("top_levels");
			top_level = top_levels.getString(0);
			// println("debug: top_level ="+top_level);
		}else{
			top_level = obj.getString("top_level");
		}
		String type = obj.getString("type");
		int group = obj.getInt("group");
		String[] split = split(url, "/");
		String id = split[split.length-1];

		Disease d = new Disease(url, name, top_level, type, group, i);
		disease_array.add(d);
		disease_map.put(name, d);
		// println(obj);

		//DiseaseGroup
		DiseaseGroup dg = (DiseaseGroup)disease_group_map.get(top_level);
		if(dg == null){
			dg = new DiseaseGroup(top_level);
			disease_group_map.put(top_level, dg);
			disease_group_array.add(dg);
		}
		dg.array.add(d);
	}

	//parse links
	source_set = new HashSet<String>();
	link_array = new ArrayList<Link>();
	for(int i = 0; i < links.size(); i++){
		JSONObject obj = links.getJSONObject(i);
		String datasources = obj.getString("datasources");
		int value = obj.getInt("value");
		int source_index = obj.getInt("source");
		int target_index = obj.getInt("target");
		Gene gene = gene_array.get(source_index);
		Disease disease = disease_array.get(target_index);
		
		// println(obj);

		//remove []''
		datasources = datasources.replace("[", "").replace("]", "").replace("'", "");
		String[] sources = split(datasources, ",");

		HashMap<String, Integer> source_counter = new HashMap<String, Integer>();
		if(sources.length >0){
			for(int j = 0; j < sources.length; j++){
				String[] split = split(sources[j], "_");
				String source = split[1];
				if(split.length>2){
					for(int k = 2; k<split.length; k++){
						source += "_"+split[k];
					}
				}
				Integer counter = (Integer) source_counter.get(source);
				if(counter == null){
					counter = new Integer(0);
					source_counter.put(source, counter);
				}
				counter = new Integer(counter.intValue() +1);
				source_counter.put(source, counter);

				//hash set
				source_set.add(source);
			}
		}else{
			println("Error: no data source indication:"+obj);
		}
		Link link = new Link(gene, disease, source_counter);
		link.total_hit_count = sources.length;
		//save link in Gene and Disease obj
		gene.links.add(link);
		disease.links.add(link);
		link_array.add(link);

		// source_set.add(link.source);

		// println(datasources+" gene:"+gene.name+"  disease:"+disease.name);
		// JSONObject sources = obj.getJSONObject("datasources");
		// println("debug: source ="+sources.size());
		// println(obj);
	}

	println("debug: source set:"+source_set);
}

void updateTreemap(){
	//make a map
	firstMap = new EvidenceMap();
	for(Link l : selected_evidence){
		if(current_lens == L_TYPE || second_lens == L_TYPE){
			//go through hashmap
			Iterator i = l.datasource_map.entrySet().iterator(); 
			while (i.hasNext()) { 
				Map.Entry me = (Map.Entry)i.next(); 
				String source_name = (String) me.getKey();
				Integer source_value  = (Integer)me.getValue();
				int value = source_value.intValue();

				if(current_lens == second_lens){
					firstMap.addItem(source_name, source_name, value);
				}else if(current_lens == L_TYPE){
					//only the first one is L_TYPE
					String second = getItemName(l, second_lens);
					firstMap.addItem(source_name, second, value);
				}else if(second_lens == L_TYPE){
					String first = getItemName(l, current_lens);
					firstMap.addItem(first, source_name, 1, value);
				}
			}
			
		}else{
			// firstMap.addItem(getItemName(l, current_lens));
			String first = getItemName(l, current_lens);
			String second = getItemName(l, second_lens);
			//weight the count based on the hit count
			if(current_lens == G_NAME_W || current_lens == D_NAME_W){
				int value = l.total_hit_count;
				firstMap.addItem(first, second, value);
			}else{
				firstMap.addItem(first, second);
			}
		}
	}
	firstMap.finishAdd();
	//finishAdd() content
	for(Mappable item: firstMap.getItems()){
		EvidenceItem ei = (EvidenceItem)item;
		ei.contents.finishAdd();
		// ei.contents.map = new Treemap(ei.contents, (double)ei.x, (double)ei.y, (double)_map_rect.width-1, (double)_map_rect.height-1);
	}

	//create treemap with firstMap
	try{
		map = new Treemap(firstMap, (double)_map_rect.x, (double)_map_rect.y, (double)_map_rect.width-1, (double)_map_rect.height-1);

	}catch(ArrayIndexOutOfBoundsException e){
		println("debug: array index out of bounds:"+firstMap.getItems().length + " selected_link size="+selected_evidence.size());
	}
}

String getItemName(Link l, int index){
	switch(index) {
		case G_NAME:
			return l.gene.name;
		case G_GROUP:
			return ""+l.gene.group;
		case D_NAME:
			return l.disease.name;
		case D_TOP:
			return l.disease.top_level;
		case D_RARE:
			if(l.disease.isRare){
				return "rare";
			}else{
				return "common";
			}
		case L_TYPES:
			return ""+l.datasource_map.size();
		case L_TOTAL_HIT:
			return ""+l.total_hit_count;
		case G_NAME_W:
			return l.gene.name;
		case D_NAME_W:
			return l.disease.name;

	}
	return "";
}


void updateLayout(){
	map.setLayout(layoutAlgorithm);
	map.updateLayout();

	//update details
	for(Mappable item: firstMap.getItems()){
		EvidenceItem ei = (EvidenceItem)item;
		ei.map = new Treemap(ei.contents, (double)ei.x, (double)ei.y, (double)ei.w-1, (double)ei.h-1);
		ei.map.setLayout(layoutAlgorithm);
		ei.map.updateLayout();
	}
}

//called when a new filter setting is added
void updateSelectedEvience(){
	//set position of the filter
	int runningX = _breadcrumb_rect.x;
	int runningY = _breadcrumb_rect.y;
	int bc_height = _margin*5;

	//reset
	selected_evidence = link_array;
	for(int i = 0; i< filter_array.size(); i++){
		ArrayList<Link> selected = new ArrayList<Link>();
		FilterSetting fs = filter_array.get(i);
		for(int j = 0; j <selected_evidence.size(); j++){
			Link l = selected_evidence.get(j);
			if(fs.match(l)){
				selected.add(l);
			}
		}
		fs.filter_result = selected_evidence.size()+" > "+ selected.size();
		fs.display_rect = new Rectangle(runningX, runningY, _breadcrumb_rect.width, bc_height);
		runningY += bc_height;

		// println("debug: before filter:"+selected_evidence.size()+" after:"+selected.size());
		selected_evidence = selected;
	}

	//update treemap
	updateTreemap();
	updateLayout();
	loop();
}


void draw(){
	background(240);

	//outline
	// strokeWeight(1);
	// stroke(120);
	// noFill();
	// rect(_map_rect.x, _map_rect.y, _map_rect.width, _map_rect.height);
	// rect(_list_rect.x, _list_rect.y, _list_rect.width, _list_rect.height);


	// map.setLayout(layoutAlgorithm);
	// map.updateLayout();
	map.draw();

	//draw filter settings
	for(int i = 0; i< lens_names.length; i++){
		Rectangle r1 = lens_label_rect[i];
		Rectangle r2 = first_lens_rect[i];
		Rectangle r3 = second_lens_rect[i];
		Rectangle r_last = lens_label_rect[lens_label_rect.length-1];
		strokeWeight(1);
		stroke(120);
		fill(255);
		rect(r1.x, r1.y, r1.width, r1.height);

		//draw buttons
		stroke(120);
		fill((i==current_lens?120:200));
		if(r2.contains(mouseX, mouseY)){
			fill(80);
			textFont(font, 14);
			textAlign(LEFT, TOP);
			text(lens_description[i], r_last.x, r_last.y+r_last.height, r_last.width+r2.width + r3.width, _margin*6);

		}
		rect(r2.x, r2.y, r2.width, r2.height);

		fill((i==second_lens?120:200));
		if(r3.contains(mouseX, mouseY)){
			fill(80);
			textFont(font, 14);
			textAlign(LEFT, TOP);
			text(lens_description[i], r_last.x, r_last.y+r_last.height, r_last.width+r2.width + r3.width, _margin*6);
		}
		rect(r3.x, r3.y, r3.width, r3.height);
		//label text
		textFont(font, 20);
		textAlign(LEFT, CENTER);
		fill(20);
		text("  "+lens_names[i], r1.x, (float)r1.getCenterY());
 	}


	if(rolloverItem != null){
		// rolloverItem.contents
		//println("debug: rollover:"+rolloverItem.name+"  count="+rolloverItem.getSize());
		rolloverItem.map.draw();

		//text
		textFont(font2, 12);
		textAlign(LEFT, CENTER);
		fill(20);

		int content_count = rolloverItem.contents.getItems().length;
		String text = rolloverItem.name + " : count="+rolloverItem.getSize()+" contents count="+content_count;
		text(text, info_text_rect.x +_margin, (float)info_text_rect.getCenterY()); 

		textFont(font);
	}

	//draw filter settings
	for(int i = 0; i< filter_array.size(); i++){
		FilterSetting fs = filter_array.get(i);
		Rectangle r = fs.display_rect;
		fill(200);
		stroke(120);
		strokeWeight(1);
		rect(r.x, r.y, r.width, r.height, 5, 5, 5, 5);
		//draw text
		//lens name
		textFont(font, 12);
		textAlign(LEFT, TOP);
		fill(20);
		text(" "+fs.lens_name, r.x, r.y);
		//filter_result
		textAlign(LEFT, BOTTOM);
		text(" "+fs.filter_result, r.x, r.y+r.height);

		//filtered value
		textFont(font, 20);
		textAlign(RIGHT, CENTER);
		text(fs.filter_value+"  ", r.x+r.width, (float)r.getCenterY());


		//check if within delete section
		if(fs.contain_delete(mouseX, mouseY)){
			Rectangle r2 = fs.get_delete_rect();
			noFill();
			stroke(20);
			strokeWeight(1);
			ellipseMode(CENTER);
			ellipse((float)r2.getCenterX(), (float)r2.getCenterY(), r2.width-3, r2.height-3);
			line(r2.x+5, r2.y+5, r2.x+r2.width-5, r2.y+r2.height-5);
			line(r2.x+r2.width-5, r2.y+5, r2.x+5, r2.y+r2.height-5);
		}


	}


	if(draw_counter > draw_max){
		println("stop looping ---------");
		noLoop();
		draw_counter = 0;
	}else{
		draw_counter ++;
	}
}


void mouseMoved(){
	cursor(ARROW);
	rolloverItem = null;
	
	if(_map_rect.contains(mouseX, mouseY)){
		//find roll over item
		for(Mappable item: firstMap.getItems()){
			EvidenceItem ei = (EvidenceItem)item;
			if(ei.mouseInside()){
				rolloverItem  = ei;
				cursor(HAND);
				break;
			}
		}
	}else if(_list_rect.contains(mouseX, mouseY)){
		if(mouseX > first_lens_rect[0].x){
			if(mouseY < first_lens_rect[first_lens_rect.length-1].y+first_lens_rect[0].height){
				cursor(HAND);
			}
		}
	}else{
	}
	loop();
}

void mousePressed(){	
	if(_list_rect.contains(mouseX, mouseY)){
		//check buttons
		for(int i = 0; i < lens_names.length; i++){
			if(first_lens_rect[i].contains(mouseX, mouseY)){
				//change first lens
				current_lens = i;
				updateTreemap();
				updateLayout();
				loop();
				break;
			}else if(second_lens_rect[i].contains(mouseX, mouseY)){
				//change second lens
				second_lens = i;
				updateTreemap();
				updateLayout();
				loop();
				break;
			}
		}
	}else if(_map_rect.contains(mouseX, mouseY)){
		//click within the treemap
		if (mouseButton == LEFT){
			// if (mouseEvent.getClickCount()==2) {  // double-click
			// 	println("double-click");
			// }else {
			// println("left-click");
			// }

			//filter current selected
			FilterSetting new_filter = new FilterSetting(current_lens, lens_names[current_lens], rolloverItem.name);
			println("debug:"+new_filter.toString());
			filter_array.add(new_filter);
			updateSelectedEvience();

		}else if (mouseButton == RIGHT){
			println("right");
		}
	}else if(_breadcrumb_rect.contains(mouseX, mouseY)){
		for(int i = 0; i< filter_array.size(); i++){
			FilterSetting fs = filter_array.get(i);
			if(fs.contain_delete(mouseX, mouseY)){
				//remove the filter settin
				filter_array.remove(i);
				updateSelectedEvience();
				loop();
				return;
			}
		}
	}
}

void mouseReleased(){
	rolloverItem = null;
}


void keyReleased() {
	// set layout algorithm
	if (key=='1') layoutAlgorithm = new SquarifiedLayout();
	if (key=='2') layoutAlgorithm = new PivotBySplitSize();
	if (key=='3') layoutAlgorithm = new SliceLayout();
	if (key=='4') layoutAlgorithm = new OrderedTreemap();
	if (key=='5') layoutAlgorithm = new StripTreemap();

	if (keyCode == DELETE || keyCode == BACKSPACE){
		// println("Delete");
		if(filter_array.size()>0){
			filter_array.remove(filter_array.size()-1);
			updateSelectedEvience();
			loop();
			return;
		}
	}

	if (key=='1'||key=='2'||key=='3'||key=='4'||key=='5'||
	key=='s'||key=='S'||key=='p'||key=='P'){
		updateLayout();
		loop();
	} 
}


