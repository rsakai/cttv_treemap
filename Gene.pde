class Gene{
	String name = "";
	int group = 0;
	String acc = ""; // "ENSG0000000000"
	int index = -1;

	ArrayList<Link> links;

	Gene(String name, String acc, int group, int index){
		this.name = name;
		this.acc = acc;
		this.group = group;
		this.index = index;
		links = new ArrayList<Link>();
	}

	ArrayList<Link> getLinks(Disease d){
		ArrayList<Link> result = new ArrayList<Link>();
		for(Link l: links){
			if(l.disease == d){
				result.add(l);
			}
		}
		return result;
	}

	

	HashSet<Disease> getDiseaseArray(){
		HashSet<Disease> result = new HashSet<Disease>();
		for(Link l:links){
			result.add(l.disease);
		}
		return result;
	}

	Link getLink(Disease d, String source){
		Link result = null;
		for(Link l: links){
			if(l.disease == d && l.source.equals(source)){
				result = l;
				return result;
			}
		}
		return result;
	}
}