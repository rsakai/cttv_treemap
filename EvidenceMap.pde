class EvidenceMap extends SimpleMapModel {    
  HashMap evidences;


  EvidenceMap() {
    // super();
    evidences = new HashMap();
  }

  // used for second lens
  void addItem(String word) {
    EvidenceItem item = (EvidenceItem) evidences.get(word);
    if (item == null) {
      item = new EvidenceItem(word);
      evidences.put(word, item);

      //color for second lens
      item.c_fill = 200;
    }
    item.incrementSize();
  }

  void addItem(String word, int value) {
    EvidenceItem item = (EvidenceItem) evidences.get(word);
    if (item == null) {
      item = new EvidenceItem(word);
      evidences.put(word, item);

      //color for second lens
      item.c_fill = 200;
    }
    double c_size = item.getSize();
    item.setSize(c_size + value);
  }

  //two lens
  void addItem(String first, String second) {
    EvidenceItem item = (EvidenceItem) evidences.get(first);
    if (item == null) {
      item = new EvidenceItem(first);
      evidences.put(first, item);
    }
    item.incrementSize();

    //add second lends to item
    item.addSecondLens(second);
  }


  //two lens with specific count
  void addItem(String first, String second, int value) {
    EvidenceItem item = (EvidenceItem) evidences.get(first);
    if (item == null) {
      item = new EvidenceItem(first);
      evidences.put(first, item);
    }
    // item.incrementSize();
    double c_size = item.getSize();
    item.setSize(c_size + value);

    //add second lends to item
    item.addSecondLens(second, value);
  }


  //when the first and second lens produce different value
  void addItem(String first, String second, int v1, int v2) {
    EvidenceItem item = (EvidenceItem) evidences.get(first);
    if (item == null) {
      item = new EvidenceItem(first);
      evidences.put(first, item);
    }
    // item.incrementSize();
    double c_size = item.getSize();
    item.setSize(c_size + v1);

    //add second lends to item
    item.addSecondLens(second, v2);
  }


  void finishAdd() {
    items = new EvidenceItem[evidences.size()];
    evidences.values().toArray(items);
  }
}
