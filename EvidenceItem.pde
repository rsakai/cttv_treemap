class EvidenceItem extends SimpleMapItem{
	String name;
	int count;
	int margin = 3;

	EvidenceMap contents = null;
	Treemap map = null;

	int c_fill = 255;

	EvidenceItem(String name){
		// super();
		this.name = name;
	}

	void addSecondLens(String n){
		if(contents == null){
			contents = new EvidenceMap();
		}
		contents.addItem(n);
	}

	void addSecondLens(String n, int value){
		if(contents == null){
			contents = new EvidenceMap();
		}
		contents.addItem(n, value);
	}

	void draw(){
		// frames
		// inheritance: x, y, w, h
		strokeWeight(0.25);
		fill(c_fill);
		rect(x, y, w, h);

		// maximize fontsize in frames
		for (int i = minFontSize; i <= maxFontSize; i++) {
		  textFont(font,i);
		  if (w < textWidth(name) + margin || h < (textAscent()+textDescent()) + margin) {
		    textFont(font,i);
		    break;
		  }
		}
		// text
		fill(0);
		textAlign(CENTER, CENTER);
		text(name, x + w/2, y + h/2);
	}


	boolean mouseInside() {
	  return (mouseX > x && mouseX < x+w && 
	          mouseY > y && mouseY < y+h);    
	}
}