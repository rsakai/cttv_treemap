class Disease{
	String url = "";
	String name = "";
	String top_level = ""; //name of top level
	boolean isRare = false;
	boolean isCommon = false;
	int group = -1;
	String id = "";
	boolean isEFO = false;
	boolean isOrphanet = false;
	boolean isTopLevel  = false;
	int index = -1;

	ArrayList<Link> links;

	Disease(String url, String name, String top_level, String type, int group, int index){
		this.url = url;
		this.name = name;
		this.top_level = top_level;
		this.group = group;
		this.index = index;

		String[] split = split(url, "/");
		this.id = split[split.length-1];

		if(type.equals("CD")){
			isCommon = true;
		}else if(type.equals("RD")){
			isRare = true;
		}
		if(this.id.startsWith("EFO")){
			this.isEFO = true;
		}else if(this.id.startsWith("Orphanet")){
			this.isOrphanet = true;
		}else{
			println("debug:Disease:unknown id:"+id);
		}

		if(this.name.equals(top_level)){
			isTopLevel = true;
		}

		links = new ArrayList<Link>();
	}

	HashSet<Gene> getGeneArray(){
		HashSet<Gene> result = new HashSet<Gene>();
		for(Link l:links){
			result.add(l.gene);
		}
		return result;
	}



}